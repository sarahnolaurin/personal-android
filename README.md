# READ ME

####General Information

######This repository will be used for doing the upcoming assignments. The application will involve one of my personal projects.
This project is a dictionary of connotations, based on user input.

---
#### Repo Structure

`src` contains the application's source code

`lib` contains the librairies

`doc` contains documentation and references

`NetBeansProjects` contains Java application tests

---
####Challenges
The implementation of the scales of the votes and linking the database correctly will be difficult because of the many quirks of Android Studio. I will have to find workarounds. 