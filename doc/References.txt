
--------------
Assignment 2
--------------
Android SQLiteAssetHelper
https://github.com/jgilfelt/android-sqlite-asset-helper

Card-Based layout
https://developer.android.com/guide/topics/ui/layout/cardview.html#create_cards_cardview

Options Menu
https://developer.android.com/guide/topics/ui/menus.html#options-menu
https://developer.android.com/training/appbar/actions.html

---
Progress Bar custom view
https://guides.codepath.com/android/progress-bar-custom-view
https://guides.codepath.com/android/handling-progressbars
https://developer.android.com/reference/android/widget/ProgressBar.html

VS

Seekbar
https://developer.android.com/reference/android/widget/SeekBar.html
---

Search bar
https://developer.android.com/guide/topics/search/search-dialog.html

Add Views dynamically in Java
https://stackoverflow.com/questions/2395769/how-to-programmatically-add-views-to-views

Check if a string contains numbers
https://stackoverflow.com/questions/18590901/check-if-a-string-contains-numbers-java/18590949

Checkable menu items
https://developer.android.com/guide/topics/ui/menus.html#checkable
Menu Items
https://developer.android.com/reference/android/view/MenuItem.html

Get drawables
https://stackoverflow.com/questions/29041027/android-getresources-getdrawable-deprecated-api-22

Databases
https://github.com/jgilfelt/android-sqlite-asset-helper
https://developer.android.com/reference/android/database/sqlite/SQLiteOpenHelper.html
https://developer.android.com/reference/android/database/Cursor.html

RecyclerView
https://guides.codepath.com/android/Using-the-RecyclerView

HashMap
https://developer.android.com/reference/java/util/HashMap.html
https://beginnersbook.com/2013/12/hashmap-in-java-with-example/

ContextMenu
https://developer.android.com/reference/android/view/ContextMenu.html
https://developer.android.com/reference/android/app/Activity.html#registerForContextMenu(android.view.View)
Menus
https://developer.android.com/guide/topics/ui/menus.html

Copying text with ClipManager
https://stackoverflow.com/questions/19253786/how-to-copy-text-to-clip-board-in-android

Swipe gesture
https://guides.codepath.com/android/implementing-pull-to-refresh-guide#using-swiperefreshlayout

--------------
Assignment 3
--------------

Button styles
https://material.io/guidelines/components/buttons.html#buttons-toggle-buttons

Flat button style
https://stackoverflow.com/questions/33620907/android-create-a-flat-button 

Relative Layout attributes
https://developer.android.com/guide/topics/ui/layout/relative.html 

Converting Set<String> to String[]
https://stackoverflow.com/questions/5982447/how-to-convert-setstring-to-string

Text-to-Speech
https://android-developers.googleblog.com/2009/09/introduction-to-text-to-speech-in.html 

Custom Views
https://developer.android.com/guide/topics/ui/custom-components 
https://kylewbanks.com/blog/drawing-triangles-rhombuses-and-other-shapes-on-android-canvas

Search bar
https://guides.codepath.com/android/Extended-ActionBar-Guide#adding-searchview-to-actionbar

To get the data from the pasred JSON in Volley
https://stackoverflow.com/questions/28120029/how-can-i-return-value-from-function-onresponse-of-volley
