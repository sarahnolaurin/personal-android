package sarah.connotation;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;

import sarah.connotation.controller.BookmarkController;
import sarah.connotation.controller.ConnotationController;
import sarah.connotation.model.BookmarkModel;
import sarah.connotation.model.Connotations;
import sarah.connotation.view.MainActivity;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    private static final String TAG = ExampleInstrumentedTest.class.getSimpleName();

    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("sarah.connotation", appContext.getPackageName());
    }

    @Test
    public void testBookmarkController() {
        Log.i(TAG,"Testing bookmark controller....");
        // Get the context of the application.
        Context context = InstrumentationRegistry.getTargetContext();
        BookmarkController bookmarkController = new BookmarkController(context);
        ArrayList<BookmarkModel> bookmarks = bookmarkController.getBookmarks();

        for(int i=0;i<bookmarks.size();i++){
            Log.wtf(TAG,bookmarks.get(i).getWord());
        }
    }
    @Test
    public void testAddBookmark() {
        Log.i(TAG,"Testing bookmark controller....");
        // Get the context of the application.
        Context context = InstrumentationRegistry.getTargetContext();
        BookmarkController bookmarkController = new BookmarkController(context);
        bookmarkController.addBookmark("6");

    }

    @Test //crashes
    public void testGetSearchResults() {
        Log.i(TAG,"Testing connotation controller....");
        // Get the context of the application.
        Context context = InstrumentationRegistry.getTargetContext();
        ConnotationController connotationController = new ConnotationController(context);
        Connotations connotations = connotationController.getSearchResults("hefty");

//        for(int i=0;i<connotations.getConnotationVotes().size();i++){
//            Log.wtf(TAG,connotations.getConnotationVotes().keySet().toString()+" \n"
//                    +connotations.getConnotationVotes().values().toString()+"");
//        }

        System.out.println("word: "+connotations.getWord());
        System.out.println("def : "+connotations.getDefinition());
    }
}
