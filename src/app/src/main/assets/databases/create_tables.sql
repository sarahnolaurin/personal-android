BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS `strength` (
	`root`	INTEGER,
	`wordId`	INTEGER,
	`vote`	INTEGER NOT NULL,
	FOREIGN KEY(`wordId`) REFERENCES `dictionary`(`wordId`),
	PRIMARY KEY(`root`,`wordId`)
);


CREATE TABLE IF NOT EXISTS `language` (
	`languageId`	INTEGER,
	`language`	TEXT,
	PRIMARY KEY(`languageId`)
);


CREATE TABLE IF NOT EXISTS `dictionary` (
	`wordId`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`word`	TEXT NOT NULL UNIQUE,
	`definition`	TEXT NOT NULL
);
CREATE TABLE "bookmarks" (
 `bookmarkId` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
 `wordId` INTEGER NOT NULL, FOREIGN KEY(`wordId`) REFERENCES `dictionary`(`wordId`)
 );
COMMIT;