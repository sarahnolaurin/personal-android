BEGIN TRANSACTION;
INSERT INTO `strength` (root,wordId,vote) VALUES (1,10,45);
INSERT INTO `strength` (root,wordId,vote) VALUES (1,8,2);

INSERT INTO `language` (languageId,language) VALUES (1,'English');
INSERT INTO `language` (languageId,language) VALUES (2,'French');
INSERT INTO `dictionary` (wordId,word,definition) VALUES (1,'hefty','of considerable weight and size');
INSERT INTO `dictionary` (wordId,word,definition) VALUES (2,'Argus-eyed','vigilant, referring to Argos, a Greek mythological watchman with a hundred eyes');
INSERT INTO `dictionary` (wordId,word,definition) VALUES (3,'pelter','a thrower of missiles');
INSERT INTO `dictionary` (wordId,word,definition) VALUES (4,'concretize','make something concrete');
INSERT INTO `dictionary` (wordId,word,definition) VALUES (5,'lubberly','clumsy and unskilled');
INSERT INTO `dictionary` (wordId,word,definition) VALUES (6,'cordial','politely warm and friendly');
INSERT INTO `dictionary` (wordId,word,definition) VALUES (7,'jocular','characterized by jokes and good humor');
INSERT INTO `dictionary` (wordId,word,definition) VALUES (8,'subnormality','the state of being less than normal');
INSERT INTO `dictionary` (wordId,word,definition) VALUES (9,'parvenu','a person who has suddenly risen to a higher economic status');
INSERT INTO `dictionary` (wordId,word,definition) VALUES (10,'heavy','of great weight; difficult to lift or move.');

INSERT INTO `bookmarks` (bookmarkId,wordId) VALUES (1,2);
INSERT INTO `bookmarks` (bookmarkId,wordId) VALUES (2,9);
INSERT INTO `bookmarks` (bookmarkId,wordId) VALUES (3,6);

COMMIT;