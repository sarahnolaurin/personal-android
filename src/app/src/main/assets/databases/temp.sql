BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS `strength` (
	`root`	INTEGER,
	`wordId`	INTEGER,
	`vote`	INTEGER NOT NULL,
	FOREIGN KEY(`wordId`) REFERENCES `dictionary`(`wordId`),
	PRIMARY KEY(`root`,`wordId`)
);
INSERT INTO `strength` (root,wordId,vote) VALUES (1,10,45);
INSERT INTO `strength` (root,wordId,vote) VALUES (1,8,2);
CREATE TABLE IF NOT EXISTS `language` (
	`languageId`	INTEGER,
	`language`	TEXT,
	PRIMARY KEY(`languageId`)
);
INSERT INTO `language` (languageId,language) VALUES (1,'English');
INSERT INTO `language` (languageId,language) VALUES (2,'French');
CREATE TABLE IF NOT EXISTS `dictionary` (
	`wordId`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`word`	TEXT NOT NULL UNIQUE,
	`definition`	TEXT NOT NULL
);
INSERT INTO `dictionary` (wordId,word,definition) VALUES (1,'hefty','of considerable weight and size');
INSERT INTO `dictionary` (wordId,word,definition) VALUES (2,'Argus-eyed','vigilant, referring to Argos, a Greek mythological watchman with a hundred eyes');
INSERT INTO `dictionary` (wordId,word,definition) VALUES (3,'pelter','a thrower of missiles');
INSERT INTO `dictionary` (wordId,word,definition) VALUES (4,'concretize','make something concrete');
INSERT INTO `dictionary` (wordId,word,definition) VALUES (5,'lubberly','clumsy and unskilled');
INSERT INTO `dictionary` (wordId,word,definition) VALUES (6,'cordial','politely warm and friendly');
INSERT INTO `dictionary` (wordId,word,definition) VALUES (7,'jocular','characterized by jokes and good humor');
INSERT INTO `dictionary` (wordId,word,definition) VALUES (8,'subnormality','the state of being less than normal');
INSERT INTO `dictionary` (wordId,word,definition) VALUES (9,'parvenu','a person who has suddenly risen to a higher economic status');
INSERT INTO `dictionary` (wordId,word,definition) VALUES (10,'heavy','of great weight; difficult to lift or move.');
CREATE TABLE IF NOT EXISTS `bookmarks` (
	`bookmarkId`	INTEGER NOT NULL,
	`wordId`	INTEGER NOT NULL,
	FOREIGN KEY(`wordId`) REFERENCES `dictionary`(`wordId`),
	PRIMARY KEY(`bookmarkId`)
);
INSERT INTO `bookmarks` (bookmarkId,wordId) VALUES (1,2);
INSERT INTO `bookmarks` (bookmarkId,wordId) VALUES (2,9);
INSERT INTO `bookmarks` (bookmarkId,wordId) VALUES (3,6);
COMMIT;
