package sarah.connotation.controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import sarah.connotation.database.DatabaseAccessHelper;
import sarah.connotation.model.BookmarkModel;

public class BookmarkController {
    private SQLiteDatabase database;
    private static DatabaseAccessHelper databaseAccessHelper;
    private String DB_TABLE_BOOKMARK = "bookmarks";
    private String DB_TABLE_DICTIONARY = "dictionary";
    private static final String TAG = BookmarkController.class.getSimpleName();


    /**
     * Private constructor to avoid object creation from outside classes.
     * @param context
     */
    public BookmarkController(Context context) {
        this.databaseAccessHelper = DatabaseAccessHelper.getInstance(context);
    }


    /**
     * Read all bookmarks from the database.
     * @return an array of bookmarks
     */
    public ArrayList<BookmarkModel> getBookmarks() {
        ArrayList<BookmarkModel> list = new ArrayList<BookmarkModel>();
        this.database = this.databaseAccessHelper.open();
        try {
            Cursor cursor = database.rawQuery("SELECT * FROM dictionary d, bookmarks b WHERE d.wordId=b.wordId;", null);
            cursor.moveToFirst();
            while(!cursor.isAfterLast()) {
                BookmarkModel bookmark = new BookmarkModel();
                bookmark.setWord(cursor.getString(cursor.getColumnIndex("word")));
                bookmark.setWordId(cursor.getInt(cursor.getColumnIndex("wordId")));
                bookmark.setBookmarkId(cursor.getInt(cursor.getColumnIndex("bookmarkId")));
                //-- Add the newly created bookmark to the list.
                list.add(bookmark);
                cursor.moveToNext();
            }
            cursor.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            this.database.close();
        }
        return list;
    }

    /**
     * Deletes a specified bookmark.
     * @param wordId the id of the word to remove
     */
    public void removeBookMark(int wordId) {
        database = this.databaseAccessHelper.open();
        try {
            database.delete(DB_TABLE_BOOKMARK, "wordId = ?", new String[]{wordId+""});
        } catch (Exception e) {
            Log.wtf(TAG, e.fillInStackTrace());
        } finally {
            database.close();
        }
    }

    /**
     * Adds a bookmark.
     * @param wordId the id of the word to add
     */
    public void addBookmark(String wordId) {
        database = this.databaseAccessHelper.open();
        database.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put("wordId", wordId );

            database.insert(DB_TABLE_BOOKMARK, null, values);
            database.setTransactionSuccessful();
        } catch (Exception e) {
            Log.wtf(TAG, e.fillInStackTrace());
        } finally {
            database.endTransaction();
            this.databaseAccessHelper.close();
        }

    }

    /**
     * Adds a bookmark.
     * @param wordId the id of the word to add
     */
    public void addBookmark(int wordId) {
        database = this.databaseAccessHelper.open();
        database.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put("wordId", wordId );

            database.insert(DB_TABLE_BOOKMARK, null, values);
            database.setTransactionSuccessful();
        } catch (Exception e) {
            Log.wtf(TAG, e.fillInStackTrace());
        } finally {
            database.endTransaction();
            this.databaseAccessHelper.close();
        }

    }

    /**
     * Find the wordId associated to the string word input voted.
     * @param word the word we want to have its id
     * @param isRoot determines whether we receive the root word or the connotation word
     * @return the wordId the word's id
     */
    public int getWordNumberAssociatedToWordName(String word, boolean isRoot) {
        database = this.databaseAccessHelper.open();
        int wordId = -1;
        try {
            Cursor cursor;
            if(isRoot)
                cursor = database.rawQuery("SELECT wordId, word FROM dictionary, strength WHERE dictionary.wordId=strength.root;", null);
            else
                cursor = database.rawQuery("SELECT wordId, word FROM dictionary, strength WHERE dictionary.wordId=strength.wordId;", null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast() && !word.equals(cursor.getString(cursor.getColumnIndex("word")))) {
                cursor.moveToNext();
            }
            wordId = cursor.getInt(cursor.getColumnIndex("wordId"));
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        finally {
            this.databaseAccessHelper.close();
        }
        return wordId;
    }

}
