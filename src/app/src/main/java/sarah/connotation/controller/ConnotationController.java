package sarah.connotation.controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import sarah.connotation.database.DatabaseAccessHelper;
import sarah.connotation.model.Connotations;
import sarah.connotation.service.DictionaryAPI;
import sarah.connotation.service.DictionaryAPICallback;

public class ConnotationController {
    private SQLiteDatabase database;
    private static DatabaseAccessHelper databaseAccessHelper;
    private String DB_TABLE_DICTIONARY = "dictionary";
    private String DB_TABLE_STRENGTH = "strength";
    private static final String TAG = ConnotationController.class.getSimpleName();
    private Context context;

    /**
     * Private constructor to avoid object creation from outside classes.
     * @param context the context
     */
    public ConnotationController(Context context) {
        this.databaseAccessHelper = DatabaseAccessHelper.getInstance(context);
        this.context = context;
    }
    /**
     * Read connotation search results.
     * @param word the word that is searched
     */
    public Connotations getSearchResults(String word) {
        Connotations connotation = new Connotations();
        try {
            //fetchAndStoreAPIData(word); (CANNOT CLOSE THE DATABASE IN TIME FOR THIS)
            database = this.databaseAccessHelper.open();
            Cursor cursor = database.rawQuery("SELECT * FROM " + DB_TABLE_DICTIONARY + " WHERE word=?", new String[]{word}); //BUT NOT THIS
            cursor.moveToFirst();

            connotation.setWordId(cursor.getInt(cursor.getColumnIndex("wordId")));
            connotation.setWord(cursor.getString(cursor.getColumnIndex("word")));
            connotation.setDefinition(cursor.getString(cursor.getColumnIndex("definition")));
            int wordId = connotation.getWordId();

            cursor = database.rawQuery("SELECT strength.root,strength.wordId,dictionary.word,strength.vote FROM dictionary, strength WHERE strength.root="+wordId+" AND dictionary.wordId=strength.wordId",null);
            cursor.moveToFirst();
            try {
                HashMap<String,Integer> scales = new HashMap<>();
                while (!cursor.isAfterLast()) {
                    String wordVoted = cursor.getString(cursor.getColumnIndex("word"));
                    int votes = cursor.getInt(cursor.getColumnIndex("vote"));
                    scales.put(wordVoted,votes);
                    cursor.moveToNext();
                }
                connotation.setConnotationVotes(scales);
            }
            catch(Exception e){
                Log.wtf(TAG,"Could not get HashMap scales.");
            }
        }
        catch (Exception e) {
            e.fillInStackTrace();
        }
        finally {
            this.databaseAccessHelper.close();
        }
        return connotation;
    }

    /**
     * Uses the API controller to request the corresponding word and then sends the result to be stored.
     * @param word the searched word
     * @throws IOException
     */
    /*public void fetchAndStoreAPIData(String word) throws IOException {
        DictionaryAPI dictionaryAPI = new DictionaryAPI(word);
        dictionaryAPI.setContext(context);
        dictionaryAPI.sendRequest(new DictionaryAPICallback() {
            @Override
            public void onSuccess(String word, String definition) {
                try {
                    insertNewWord(word,definition);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }*/

    /**
     * Inserts a new word with its definition to the database
     * @param word the selected word
     * @param definition the associated definition
     */
    public void insertNewWord(String word, String definition) {
        try {
            database = this.databaseAccessHelper.open();
            database.beginTransaction();
            ContentValues values = new ContentValues();
            values.put("word", word);
            values.put("definition", definition);
            database.insert(DB_TABLE_DICTIONARY, null, values );
            database.setTransactionSuccessful();
            Log.wtf(TAG,"insert");
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            this.databaseAccessHelper.close();
        }
    }

    /**
     * Read connotation search results of a random word.
     * @param isRandom determines if we are doing a random search or not
     */
    public Connotations getSearchResults(boolean isRandom) {
        Connotations connotation = new Connotations();
        database = this.databaseAccessHelper.open();
        try {
            Cursor cursor = database.rawQuery("SELECT * FROM " + DB_TABLE_DICTIONARY , null);
            Random rand = new Random();
            int wordId = rand.nextInt(cursor.getCount());
            cursor.move(wordId);
            String word = cursor.getString(cursor.getColumnIndex("word"));
            connotation = getSearchResults(word);
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        finally {
            this.database.close();
        }
        return connotation;
    }

    /**
     * Add a vote for the given word to the database.
     * @param root is the given wordId
     * @param wordId is the wordId that a vote is going to be added to it.
     */
    public void addVote(int root, int wordId) {
        database = this.databaseAccessHelper.open();
        database.beginTransaction();
        try {
            Cursor cursor = database.rawQuery("SELECT * FROM " + DB_TABLE_STRENGTH+" WHERE rootId="+root+" and wordId="+wordId+";", null);
            if(cursor != null) { //if the row already exists
                cursor.moveToFirst();
                int votes = cursor.getInt(cursor.getColumnIndex("vote"));
                votes++;

                ContentValues values = new ContentValues();
                values.put("rootId", root);
                values.put("wordId", wordId);
                values.put("vote", votes);
                database.update(DB_TABLE_STRENGTH, values, "rootId=? and wordId=?", new String[]{root + "", wordId + ""});
            }
            else { //if the row does not exist
                int votes = 1;
                ContentValues values = new ContentValues();
                values.put("rootId", root);
                values.put("wordId", wordId);
                values.put("vote", votes);
                database.insert(DB_TABLE_STRENGTH, null, values);
                database.setTransactionSuccessful();
            }
        }
        catch(Exception e) {
            e.fillInStackTrace();
        }
        finally {
            database.endTransaction();
            this.databaseAccessHelper.close();
        }
    }

    /**
     * Receive a list of all the words in the database.
     * @return a list of the words in the database.
     */
    public ArrayList<String> getWordList() {
        ArrayList<String> words = new ArrayList<>();
        database = this.databaseAccessHelper.open();
        try {
            Cursor cursor = database.rawQuery("SELECT word FROM "+DB_TABLE_DICTIONARY+";",null);
            cursor.moveToFirst();
            while(!cursor.isAfterLast()){
                words.add(cursor.getString(cursor.getColumnIndex("word")));
                cursor.moveToNext();
            }
        }
        catch(Exception e) {
            e.fillInStackTrace();
        }
        finally {
            this.databaseAccessHelper.close();
        }
        return words;
    }

    /**
     * Find the wordId associated to the string word input voted.
     * @param word the word we want to have its id
     * @param isRoot determines whether we receive the root word or the connotation word
     * @return the wordId
     */
    public int getWordNumberAssociatedToWordName(String word, boolean isRoot) {
        database = this.databaseAccessHelper.open();
        int wordId = -1;
        try {
            Cursor cursor;
            if(isRoot)
                cursor = database.rawQuery("SELECT wordId, word FROM dictionary, strength WHERE dictionary.wordId=strength.root;", null);
            else
                cursor = database.rawQuery("SELECT wordId, word FROM dictionary, strength WHERE dictionary.wordId=strength.wordId;", null);
//            cursor = database.rawQuery("SELECT wordId FROM "+DB_TABLE_DICTIONARY+";",null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast() && !word.equals(cursor.getString(cursor.getColumnIndex("word")))) {
                cursor.moveToNext();
            }
            wordId = cursor.getInt(cursor.getColumnIndex("wordId"));
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        finally {
            this.databaseAccessHelper.close();
        }
        return wordId;
    }

}
