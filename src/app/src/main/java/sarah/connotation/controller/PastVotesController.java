package sarah.connotation.controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

import sarah.connotation.database.DatabaseAccessHelper;
import sarah.connotation.model.PastVotesModel;

public class PastVotesController {

    private SQLiteDatabase database;
    private static DatabaseAccessHelper databaseAccessHelper;
    private String DB_TABLE_STRENGTH = "strength";
    private static final String TAG = PastVotesController.class.getSimpleName();

    /**
     * Private constructor to avoid object creation from outside classes.
     * @param context
     */
    public PastVotesController(Context context) {
        this.databaseAccessHelper = DatabaseAccessHelper.getInstance(context);
    }

    //MODIFY DATABASE SO THAT WE SEE PAST VOTES ONLY ASSOCIATED TO THE PERSON'S 'PROFILE', CACHED DATA OR SOMETHING ELSE
    /**
     * Read past votes.
     * @return a HashMap object
     */
    public ArrayList<PastVotesModel> getPastVotes() {
        ArrayList<PastVotesModel> rootVotes = new ArrayList<>();
        try {
            database = this.databaseAccessHelper.open();
            Cursor cursor = database.rawQuery("SELECT * FROM "+DB_TABLE_STRENGTH+";",null);
            cursor.moveToFirst();
            while(!cursor.isAfterLast()) {
                String root = getWordNameAssociatedToWordNumber(cursor.getInt(cursor.getColumnIndex("root")),true);
                if(!rootVotes.contains(root)) {
                    String vote = getWordNameAssociatedToWordNumber(cursor.getInt(cursor.getColumnIndex("wordId")), false);
                    rootVotes.add(new PastVotesModel(root, vote));
                }
                cursor.moveToNext();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            this.databaseAccessHelper.close();
        }
        return rootVotes;
    }

    /**
     *  Delete all past votes.
     */
    public void deleteVoteHistory() {
        database = this.databaseAccessHelper.open();
        try {
            database.delete(DB_TABLE_STRENGTH, "",null);
        } catch (Exception e) {
            Log.wtf(TAG, e.fillInStackTrace());
        } finally {
            database.close();
        }
    }

    /**
     * Modify a past vote for up to a year.
     * @param oldVote the vote to be modified
     * @param newVote the vote to be stored and replace the old vote
     */
    public void updateVote(String oldVote, String newVote) {
        if(newVote.length() > 0) {
            database = this.databaseAccessHelper.open();
            try {
                int wordId = getWordNumberAssociatedToWordName(oldVote, false);
                Cursor cursor = database.rawQuery("SELECT * FROM " + DB_TABLE_STRENGTH + " WHERE wordId=" + wordId + ";", null);
                cursor.moveToFirst();
                int vote = cursor.getInt(cursor.getColumnIndex("vote"));
                int root = cursor.getInt(cursor.getColumnIndex("root"));
                ContentValues values = new ContentValues();
                values.put("root", root);
                values.put("wordId", getWordNumberAssociatedToWordName(newVote, false));
                values.put("vote", vote);
                database.update(DB_TABLE_STRENGTH, values, "wordId = ?", new String[]{});
            } catch (Exception e) {
                Log.wtf(TAG, e.fillInStackTrace());
            } finally {
                databaseAccessHelper.close();
            }
        }
    }

    /**
     * Find the wordId associated to the string word input voted.
     * @param word the word we want to have its id
     * @param isRoot determines whether we receive the root word or the connotation word
     * @return the wordId
     */
    public int getWordNumberAssociatedToWordName(String word, boolean isRoot) {
        database = this.databaseAccessHelper.open();
        int wordId = -1;
        try {
            Cursor cursor;
            if(isRoot)
                cursor = database.rawQuery("SELECT wordId, word FROM dictionary, strength WHERE dictionary.wordId=strength.root;", null);
            else
                cursor = database.rawQuery("SELECT wordId, word FROM dictionary, strength WHERE dictionary.wordId=strength.wordId;", null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast() && !word.equals(cursor.getString(cursor.getColumnIndex("word")))) {
                cursor.moveToNext();
            }
            wordId = cursor.getInt(cursor.getColumnIndex("wordId"));
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        finally {
            this.databaseAccessHelper.close();
        }
        return wordId;
    }

    /**
     * Find the word associated to the primary key wordId.
     * @param wordId the word id we want to have its string word
     * @param isRoot determines whether we receive the root word or the connotation word
     * @return the wordId
     */
    public String getWordNameAssociatedToWordNumber(int wordId, boolean isRoot) {
        database = this.databaseAccessHelper.open();
        String word = "";
        try {
            Cursor cursor;
            if(isRoot)
                cursor = database.rawQuery("SELECT wordId, word FROM dictionary d, strength s WHERE d.wordId=s.root;", null);
            else
                cursor = database.rawQuery("SELECT wordId, word FROM dictionary d, strength s WHERE d.wordId=s.wordId;", null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast() && !word.equals(cursor.getString(cursor.getColumnIndex("word")))) {
                cursor.moveToNext();
            }
            word = cursor.getString(cursor.getColumnIndex("word"));
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        finally {
            this.databaseAccessHelper.close();
        }
        return word;
    }
}
