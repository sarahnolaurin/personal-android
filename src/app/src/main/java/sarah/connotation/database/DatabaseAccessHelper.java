package sarah.connotation.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DatabaseAccessHelper {
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;
    private static DatabaseAccessHelper instance;

    /**
     * Private constructor to avoid object creation from outside classes.
     * @param context the context
     */
    private DatabaseAccessHelper(Context context) {
        this.openHelper = new DatabaseOpenHelper(context);
    }

    /**
     * Return a single instance of DatabaseAccessHelper.
     * @param context the Context
     * @return the instance of DatabaseAccessHelper
     */
    public static synchronized DatabaseAccessHelper getInstance(Context context) {
        if(instance == null) {
            instance = new DatabaseAccessHelper(context);
        }
        return instance;
    }

    /**
     * Open the database connection.
     */
    public SQLiteDatabase open() {

        this.database = openHelper.getWritableDatabase();
        return this.database;
    }

    /**
     * Close the database connection.
     */
    public void close() {
        if (database != null)
            this.database.close();
    }
}
