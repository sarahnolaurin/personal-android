package sarah.connotation.model;


public class BookmarkModel {
    private  String word; //the bookmarked word (taken from word id)
    private  int wordId;//the word id
    private  int bookmarkId; //the bookmarked word id

    public BookmarkModel() {
    }

    public BookmarkModel(String word, int wordId, int bookmarkId) {
        this.word = word;
        this.wordId = wordId;
        this.bookmarkId = bookmarkId;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public int getWordId() {
        return wordId;
    }

    public void setWordId(int wordId) {
        this.wordId = wordId;
    }

    public int getBookmarkId() {
        return bookmarkId;
    }

    public void setBookmarkId(int bookmarkId) {
        this.bookmarkId = bookmarkId;
    }

    @Override
    public String toString() {
        return "BookmarkModel{" +
                "word='" + word + '\'' +
                ", wordId=" + wordId +
                ", bookmarkId=" + bookmarkId +
                '}';
    }
}
