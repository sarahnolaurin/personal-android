package sarah.connotation.model;


import java.util.HashMap;

public class Connotations {
    //connotations for one word
    private int wordId;  //the root id
    private String word; //the root word
    private String definition;
    private HashMap<String,Integer> connotationVotes; //word connotations with their votes
    //be able to identify votes for one person/profile

    public Connotations() {

    }

    public int getWordId() {
        return wordId;
    }

    public void setWordId(int wordId) {
        this.wordId = wordId;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public HashMap<String, Integer> getConnotationVotes() {
        return connotationVotes;
    }

    public void setConnotationVotes(HashMap<String, Integer> connotationVotes) {
        this.connotationVotes = connotationVotes;
    }

}
