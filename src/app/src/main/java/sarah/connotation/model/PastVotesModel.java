package sarah.connotation.model;

public class PastVotesModel {
    private String wordGiven;
    private String wordVoted;

    public PastVotesModel() {
    }

    public PastVotesModel(String wordGiven, String wordVoted) {
        this.wordGiven = wordGiven;
        this.wordVoted = wordVoted;
    }

    public String getWordGiven() {
        return wordGiven;
    }

    public void setWordGiven(String wordGiven) {
        this.wordGiven = wordGiven;
    }

    public String getWordVoted() {
        return wordVoted;
    }

    public void setWordVoted(String wordVoted) {
        this.wordVoted = wordVoted;
    }

    @Override
    public String toString() {
        return "PastVotesModel{" +
                "wordGiven='" + wordGiven + '\'' +
                ", wordVoted='" + wordVoted + '\'' +
                '}';
    }
}
