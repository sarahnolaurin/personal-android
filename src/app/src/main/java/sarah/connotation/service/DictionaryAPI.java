package sarah.connotation.service;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class DictionaryAPI {
    private static final String TAG = "DICTIONARYAPI";
    private String wordSearched;
    private String strUrl;
    private RequestQueue mRequestQueue;
    private Context context;

    public DictionaryAPI(String word) {
        strUrl = "https://glosbe.com/gapi/translate?from=eng&dest=eng&format=json&phrase="+word+"&pretty=true";
        this.wordSearched = word;
    }

    /**
     * Sends a request to the API to get the word and the definition.
     * @param callback Sends and stores the result in the interface callback.
     * @throws IOException in case of a JSON malfunction
     */
    public void sendRequest(final DictionaryAPICallback callback) throws IOException {
        try {
            if(isConnected()) {
                mRequestQueue = Volley.newRequestQueue(getContext());
                JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, strUrl, null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                //Log.wtf("json", response.toString());
                                if(response != null) {
                                    String word = getWordFromResponse(response);
                                    String definition = getDefinitionFromResponse(response);
                                    Log.wtf(TAG, "i get here " + definition);
                                    callback.onSuccess(word, definition);
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.e("Error: ", error.getMessage());
                    }
                });
                mRequestQueue.add(req);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    /**
     * Makes sure the app is connected to the network.
     * @return true if the app is connected
     */
    public boolean isConnected() {
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    /**
     * Parses the JSON response to get the first definition.
     * @param response the found response
     * @return the definition
     */
    public String getDefinitionFromResponse(JSONObject response){
        JSONObject meaningsObj = null;
        String meaningsKey = "meanings";
        String textKey = "text";
        String definition = "";
        final int maxDef = 90;
        try {
            JSONArray tucs = response.getJSONArray("tuc");
            if (tucs.length() > 0){
                meaningsObj = (JSONObject)tucs.get(0);
                //String meanings = (String)jsonObject.get("name");
                //Log.wtf("meanings", meanings.toString());
                //System.out.println("\n\n\nParsed meanings oobjectooooo \n: "+ meaningsObj);
                JSONArray meaningAr = (JSONArray) meaningsObj.get(meaningsKey) ;
                //System.out.println("\n\n\nParsed meaning, description:\n "+ meaningAr);
                //-- GEtting the text definition.
                JSONObject firstDefinition = (JSONObject) meaningAr.get(0);
                definition = firstDefinition.getString(textKey);
                System.out.println("\n\n\nParsed definition:\n "+ definition);
                if(definition.length() > maxDef) {
                    definition = definition.substring(0,maxDef);
                    definition += "...";
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            System.err.println(e.getMessage());
        }
        return definition;
    }

    /**
     * Parses the JSON response to get the word sent.
     * @param response the found response
     * @return the word
     */
    public String getWordFromResponse(JSONObject response) {
        String phraseKey = "phrase";
        String word = "";
        try {
            word = response.getString(phraseKey);
            System.out.println("\n\n\nParsed word:\n "+ word);
        }catch (JSONException e) {
            e.printStackTrace();
            System.err.println(e.getMessage());
        }
        return word;
    }
}
