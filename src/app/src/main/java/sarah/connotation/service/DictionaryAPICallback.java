package sarah.connotation.service;


public interface DictionaryAPICallback {
    /**
     * Will send the word and definition from the dictionary api
     * @param word the searched word
     * @param definition the associated definition
     */
    void onSuccess(String word, String definition);
}
