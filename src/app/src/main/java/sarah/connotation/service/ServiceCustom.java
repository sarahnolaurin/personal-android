package sarah.connotation.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.util.Log;

import sarah.connotation.R;
import sarah.connotation.view.MainActivity;
import sarah.connotation.view.WhyApp;
import sarah.connotation.view.secondary.VoteDialog;

public class ServiceCustom extends Service {
    private static final String channelId = "Random";
    NotificationChannel channel = new NotificationChannel(channelId, "Random Word", NotificationManager.IMPORTANCE_DEFAULT);
    public ServiceCustom() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //add a timer later for calling this
        if(intent.getAction().equals("notification")) {
            Intent actIntent = new Intent(this, MainActivity.class);
            actIntent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, actIntent, 0);

            Notification.Builder builder = new Notification.Builder(this, channelId)
                    .setContentTitle("Connotation voting!").setContentText("Please vote to help make this app better!.")
                    .setAutoCancel(false).setSmallIcon(R.drawable.ic_help)
                    .setContentIntent(pendingIntent);
            Notification notification = builder.build();

            try {
                NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                manager.createNotificationChannel(channel);
                manager.notify(1, notification);
            } catch (Exception e) {
                Log.wtf("main", "manager");
                Log.wtf("error", e.getMessage());
            }
        }
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        //  Return the communication channel to the service.
        return null;
    }
}
