package sarah.connotation.view;


import android.support.design.widget.Snackbar;
import android.content.Intent;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.nikhilpanju.recyclerviewenhanced.OnActivityTouchListener;
import com.nikhilpanju.recyclerviewenhanced.RecyclerTouchListener;

import java.util.ArrayList;

import sarah.connotation.R;
import sarah.connotation.controller.BookmarkController;
import sarah.connotation.model.BookmarkModel;
import sarah.connotation.view.recycler.BookmarkViewHolder;
import sarah.connotation.view.recycler.BookmarksAdapter;
import sarah.connotation.view.secondary.VoteDialog;

public class Bookmarks extends AppCompatActivity implements RecyclerTouchListener.RecyclerTouchListenerHelper {
    private static final String TAG = "BOOKMARKS";
    View view;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter<BookmarkViewHolder> adapter;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerTouchListener onTouchListener;
    private OnActivityTouchListener activityTouchListener;

    private SwipeRefreshLayout swipeContainer;

    private SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookmarks);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        try{
            //Find UI components
            this.recyclerView = (RecyclerView) findViewById(R.id.bookmarksRecyclerView);
            //Arrange items linearly
            this.layoutManager = new LinearLayoutManager(this);
            this.recyclerView.setLayoutManager(layoutManager);
            //Get the bookmarks
            final BookmarkController bookmarkController = new BookmarkController(this);
            final ArrayList<BookmarkModel> bookmarks = bookmarkController.getBookmarks();
            //create the adapter
            this.adapter = new BookmarksAdapter(bookmarks);
            //set the layoutManager
            this.recyclerView.setAdapter(adapter);

            // Configuring the recycler swipes
            onTouchListener = new RecyclerTouchListener(this, recyclerView);
            onTouchListener.setIndependentViews(R.id.word_bookmarked);
            onTouchListener.setViewsToFade(R.id.word_bookmarked);
            onTouchListener.setClickable(new RecyclerTouchListener.OnRowClickListener() {
                @Override
                public void onRowClicked(int position) {
                    Log.wtf(TAG, "Row "+position+" is clicked");
                    sendSearch(bookmarks.get(position).getWord());
                }
                @Override
                public void onIndependentViewClicked(int independentViewID, int position) { }
            });
            onTouchListener.setSwipeOptionViews(R.id.delete);
            onTouchListener.setSwipeable(R.id.bookmark_rowFG, R.id.bookmark_rowBG, new RecyclerTouchListener.OnSwipeOptionsClickListener() {
                @Override
                public void onSwipeOptionClicked(int viewID, int position) {
                    if(viewID == R.id.delete){
                        BookmarkModel bookmarkModel = bookmarks.get(position);
                        bookmarks.remove(position);
                        //bookmarkController.removeBookMark(position);
                        adapter = new BookmarksAdapter(bookmarks);
                        recyclerView.setAdapter(adapter);
                        makeSnackbar(bookmarks, bookmarkModel, position);
                    }
                }
            });

            swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
            swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    ArrayList<BookmarkModel> bookmarks = bookmarkController.getBookmarks();
                    adapter = new BookmarksAdapter(bookmarks);
                    recyclerView.setAdapter(adapter);
                    swipeContainer.setRefreshing(false);
                }
            });
            swipeContainer.setColorSchemeResources(R.color.lightPink,
                    R.color.TealAccent200,
                    R.color.yellow300,
                    R.color.cyanAccent200);

        }
        catch(Exception e) {
            Log.wtf(TAG,e.getMessage());
        }
    }



    public void makeSnackbar(final ArrayList<BookmarkModel> bookmarks, final BookmarkModel bookmark, int position) {
        View.OnClickListener UndoListener = new View.OnClickListener() {
            public void onClick(View view) {
                undoDelete(bookmarks,bookmark);
            }
        };

        View parentView = findViewById(R.id.bookmark_container);
        Snackbar.make(parentView,"Bookmark deleted",Snackbar.LENGTH_LONG)
                .setAction("UNDO", UndoListener)
                .setActionTextColor(getResources().getColor(R.color.yellow300,null))
                .setDuration(3000).show();
    }

    public void undoDelete(ArrayList<BookmarkModel> bookmarks, BookmarkModel bookmark) {
        BookmarkController controller = new BookmarkController(getApplicationContext());
        bookmarks.add(bookmark);
        controller.addBookmark(bookmark.getWordId());
        adapter = new BookmarksAdapter(bookmarks);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.searchItm);
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.wtf(TAG, query);
                sendSearch(query);
                searchView.clearFocus();
                return true;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        return true;
    }

    @Override
    public void onResume(){
        super.onResume();
        recyclerView.addOnItemTouchListener(onTouchListener);
    }

    @Override
    public void onPause() {
        super.onPause();
        recyclerView.removeOnItemTouchListener(onTouchListener);
    }

    @Override
    public void setOnActivityTouchListener(OnActivityTouchListener listener) {
        this.activityTouchListener = listener;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (activityTouchListener != null) activityTouchListener.getTouchCoordinates(ev);
        return super.dispatchTouchEvent(ev);
    }

    /**
     * Send the search query to the SearchResult class.
     * @param word the searched query
     */
    public void sendSearch(String word) {
        Intent intent = new Intent (this, SearchResult.class);
        if(word.length() > 0) {
            intent.putExtra("search_input", word);
        }
        else {
            sendSearch(true);
        }
        startActivity(intent);
    }

    /**
     * Send a search query for a random word
     * @param isRandom value that determines whether the search should choose a random word or not.
     */
    public void sendSearch(Boolean isRandom) {
        Intent intent = new Intent(this, SearchResult.class);
        if(isRandom)
            intent.putExtra("isRandom", true);
        startActivity(intent);
    }

    /**
     * Sends the user to a dialog fragment to vote the connotation of a word.
     */
    public void sendToVote() {
        try {
            android.support.v4.app.FragmentManager manager = getSupportFragmentManager();
            VoteDialog voteDialog = new VoteDialog();
            voteDialog.setCancelable(true);
            voteDialog.show(manager,"Vote Dialog");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Shows the received result from the dialog fragment.
     * @param voteInput the vote the user inputted
     */
    public void onFinishResultDialog(String voteInput) {
        Log.i(TAG,"Received "+voteInput);
        Toast.makeText(this, "Received "+voteInput,Toast.LENGTH_LONG).show();
        //call method/class that stores vote string
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case R.id.voteItm:
                sendToVote();
                return true;
            case R.id.bookedItm:
                sendToBookmarks(view);
                return true;
            case R.id.randItm: //sends to SearchResult activity but with a piece of data that says to randomize the word
                sendSearch(true);
                return true;
            case R.id.pastItm:
                sendToPast(view);
                return true;
            case R.id.statsItm:
                sendToStats(view);
                return true;
            case R.id.aboutItm:
                sendToAbout(view);
                return true;
            case R.id.settingItm:
                sendToSettings(view);
                return true;
            case R.id.helpItm:
                sendToHelp(view);
                return true;
            case R.id.whyItm:
                sendToWhyApp(view);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    /**
     * Sends the user to the Bookmarks activity.
     * @param view the current view
     */
    public void sendToBookmarks(View view) {
        Intent intent = new Intent(Bookmarks.this, Bookmarks.class);
        startActivity(intent);
    }

    /**
     * Sends the user to the WhyApp activity.
     * @param view the current view
     */
    public void sendToWhyApp(View view) {
        Intent intent = new Intent(Bookmarks.this, WhyApp.class);
        startActivity(intent);
    }

    /**
     * Sends the user to the Help activity.
     * @param view the current view
     */
    public void sendToHelp(View view) {
        Intent intent = new Intent(Bookmarks.this, Help.class);
        startActivity(intent);
    }

    /**
     * Sends the user to the ContactUs activity.
     * @param view the current view
     */
    public void sendToAbout(View view) {
        Intent intent = new Intent(Bookmarks.this, ContactUs.class);
        startActivity(intent);
    }

    /**
     * Sends the user to the Stats activity.
     * @param view the current view
     */
    public void sendToStats(View view) {
        Intent intent = new Intent(Bookmarks.this, Stats.class);
        //add extra data word currently on if it is already on (ex: SearchResult)
        startActivity(intent);
    }

    /**
     * Sends the user to the Settings activity.
     * @param view the current view
     */
    public void sendToSettings(View view) {
        Intent intent = new Intent(Bookmarks.this, Settings.class);
        startActivity(intent);
    }

    /**
     * Sends the user to the History activity.
     * @param view the current view
     */
    public void sendToPast(View view) {
        Intent intent = new Intent(Bookmarks.this, History.class);
        startActivity(intent);
    }
}
