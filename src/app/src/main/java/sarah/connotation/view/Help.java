package sarah.connotation.view;

import android.content.Intent;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import sarah.connotation.R;
import sarah.connotation.service.ServiceCustom;
import sarah.connotation.view.secondary.VoteDialog;

public class Help extends AppCompatActivity {
    private static final String TAG = "HELP";
    private SearchView searchView;
    View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = new Intent(this, ServiceCustom.class);
        intent.setAction("notification");
        startService(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.searchItm);
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.wtf(TAG, query);
                sendSearch(query);
                searchView.clearFocus();
                return true;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        return true;
    }

    /**
     * Send a search query for a random word
     * @param isRandom value that determines whether the search should choose a random word or not.
     */
    public void sendSearch(Boolean isRandom) {
        Intent intent = new Intent(this, SearchResult.class);
        if(isRandom)
            intent.putExtra("isRandom", true);
        startActivity(intent);
    }

    /**
     * Send the search query to the SearchResult class.
     * @param query the searched query
     */
    public void sendSearch(String query) {
        Intent intent = new Intent(this, SearchResult.class);
        if(query.length() > 0) {
            intent.putExtra("search_input", query);
        }
        else {
            sendSearch(true);
        }
        startActivity(intent);
    }

    /**
     * Sends the user to a dialog fragment to vote the connotation of a word.
     */
    public void sendToVote() {
        try {
            android.support.v4.app.FragmentManager manager = getSupportFragmentManager();
            VoteDialog voteDialog = new VoteDialog();
            voteDialog.setCancelable(true);
            //voteDialog.setDialogTitle("Add a connotation to this word");
            voteDialog.show(manager,"Vote Dialog");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Shows the received result from the dialog fragment.
     * @param voteInput the vote the user inputted
     */
    public void onFinishResultDialog(String voteInput) {
        Log.i(TAG,"Received "+voteInput);
        Toast.makeText(this, "Received "+voteInput,Toast.LENGTH_LONG).show();
        //call method/class that stores vote string
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case R.id.voteItm:
                sendToVote();
                return true;
            case R.id.bookedItm:
                sendToBookmarks(view);
                return true;
            case R.id.randItm: //sends to SearchResult activity but with a piece of data that says to randomize the word
                sendSearch(false);
                return true;
            case R.id.pastItm:
                sendToPast(view);
                return true;
            case R.id.statsItm:
                sendToStats(view);
                return true;
            case R.id.aboutItm:
                sendToAbout(view);
                return true;
            case R.id.settingItm:
                sendToSettings(view);
                return true;
            case R.id.helpItm:
                sendToHelp(view);
                return true;
            case R.id.whyItm:
                sendToWhyApp(view);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Sends the user to the Bookmarks activity.
     * @param view the current view
     */
    public void sendToBookmarks(View view) {
        Intent intent = new Intent(this, Bookmarks.class);
        startActivity(intent);
    }

    /**
     * Sends the user to the WhyApp activity.
     * @param view the current view
     */
    public void sendToWhyApp(View view) {
        Intent intent = new Intent(this, WhyApp.class);
        startActivity(intent);
    }

    /**
     * Sends the user to the Help activity.
     * @param view the current view
     */
    public void sendToHelp(View view) {
        Intent intent = new Intent(this, Help.class);
        startActivity(intent);
    }

    /**
     * Sends the user to the ContactUs activity.
     * @param view the current view
     */
    public void sendToAbout(View view) {
        Intent intent = new Intent(this, ContactUs.class);
        startActivity(intent);
    }

    /**
     * Sends the user to the Stats activity.
     * @param view the current view
     */
    public void sendToStats(View view) {
        Intent intent = new Intent(this, Stats.class);
        //add extra data word currently on if it is already on (ex: SearchResult)
        startActivity(intent);
    }

    /**
     * Sends the user to the Settings activity.
     * @param view the current view
     */
    public void sendToSettings(View view) {
        Intent intent = new Intent(this, Settings.class);
        startActivity(intent);
    }

    /**
     * Sends the user to the History activity.
     * @param view the current view
     */
    public void sendToPast(View view) {
        Intent intent = new Intent(this, History.class);
        startActivity(intent);
    }
}
