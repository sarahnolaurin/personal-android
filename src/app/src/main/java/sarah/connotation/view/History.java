package sarah.connotation.view;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.nikhilpanju.recyclerviewenhanced.OnActivityTouchListener;
import com.nikhilpanju.recyclerviewenhanced.RecyclerTouchListener;

import java.util.ArrayList;

import sarah.connotation.view.secondary.EditDialog;
import sarah.connotation.view.recycler.HistoryAdapter;
import sarah.connotation.R;
import sarah.connotation.controller.PastVotesController;
import sarah.connotation.model.PastVotesModel;
import sarah.connotation.view.recycler.HistoryViewHolder;
import sarah.connotation.view.secondary.VoteDialog;


public class History extends AppCompatActivity implements RecyclerTouchListener.RecyclerTouchListenerHelper {
    private static final String TAG = "HISTORY";
    View view;
    private SearchView searchView;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter<HistoryViewHolder> adapter;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerTouchListener onTouchListener;
    private OnActivityTouchListener activityTouchListener;

    private SwipeRefreshLayout swipeContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        try{
            this.recyclerView = (RecyclerView) findViewById(R.id.historyRecyclerView);
            this.layoutManager = new LinearLayoutManager(this);
            this.recyclerView.setLayoutManager(layoutManager);
            final PastVotesController controller = new PastVotesController(this);
            final ArrayList<PastVotesModel> pastVotes = controller.getPastVotes();
            this.adapter = new HistoryAdapter(pastVotes);
            this.recyclerView.setAdapter(adapter);
            Log.wtf(TAG,"i am before swipes");
            // Configuring the recycler swipes
            onTouchListener = new RecyclerTouchListener(this, recyclerView);
            onTouchListener.setIndependentViews(R.id.word_voted,R.id.word_given);
            onTouchListener.setViewsToFade(R.id.forWord,R.id.word_given,R.id.yourVote);
            onTouchListener.setClickable(new RecyclerTouchListener.OnRowClickListener() {
                @Override
                public void onRowClicked(int position) {
                    Log.wtf(TAG, "Row "+position+" is clicked");
                    sendSearch(pastVotes.get(position).getWordGiven());
                }
                @Override
                public void onIndependentViewClicked(int independentViewID, int position) {
                    if(independentViewID == R.id.word_voted){

                    }
                    else { //word_given

                    }
                }
            });
            onTouchListener.setSwipeOptionViews(R.id.edit);
            onTouchListener.setSwipeable(R.id.history_rowFG, R.id.history_rowBG, new RecyclerTouchListener.OnSwipeOptionsClickListener() {
                @Override
                public void onSwipeOptionClicked(int viewID, int position) {
                    if(viewID == R.id.edit){
                        sendToEdit();
                        adapter = new HistoryAdapter(pastVotes);
                        recyclerView.setAdapter(adapter);
                    }
                }
            });

            swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
            swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    ArrayList<PastVotesModel> pastVotes = controller.getPastVotes();
                    adapter = new HistoryAdapter(pastVotes);
                    recyclerView.setAdapter(adapter);
                    swipeContainer.setRefreshing(false);
                }
            });
            swipeContainer.setColorSchemeResources(R.color.cyanAccent200,
                    R.color.TealAccent200,
                    R.color.yellow300,
                    R.color.lightPink);
            }

        catch(Exception e) {
            Log.wtf(TAG,e.getMessage());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.searchItm);
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.wtf(TAG, query);
                sendSearch(query);
                searchView.clearFocus();
                return true;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        return true;
    }

    @Override
    public void onResume(){
        super.onResume();
        recyclerView.addOnItemTouchListener(onTouchListener);
    }

    @Override
    public void onPause() {
        super.onPause();
        recyclerView.removeOnItemTouchListener(onTouchListener);
    }

    @Override
    public void setOnActivityTouchListener(OnActivityTouchListener listener) {
        this.activityTouchListener = listener;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (activityTouchListener != null) activityTouchListener.getTouchCoordinates(ev);
        return super.dispatchTouchEvent(ev);
    }

    /**
     * Sends the user to edit their vote.
     */
    public void sendToEdit() {
        try {
            android.support.v4.app.FragmentManager manager = getSupportFragmentManager();
            EditDialog editDialog = new EditDialog();
            editDialog.setCancelable(true);
            //editDialog.setDialogTitle("Add a connotation to this word");
            editDialog.show(manager,"Edit Dialog");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Send the search query to the SearchResult class.
     * @param word the searched query
     */
    public void sendSearch(String word) {
        Intent intent = new Intent (this, SearchResult.class);
        if(word.length() > 0) {
            intent.putExtra("item name", word);
        }
        else {
            sendSearch(true);
        }
        startActivity(intent);
    }

    /**
     * Send a search query for a random word
     * @param isRandom value that determines whether the search should choose a random word or not.
     */
    public void sendSearch(Boolean isRandom) {
        Intent intent = new Intent(this, SearchResult.class);
        if(isRandom)
            intent.putExtra("isRandom", true);
        startActivity(intent);
    }

    /**
     * Sends the user to a dialog fragment to vote the connotation of a word.
     */
    public void sendToVote() {
        try {
            android.support.v4.app.FragmentManager manager = getSupportFragmentManager();
            VoteDialog voteDialog = new VoteDialog();
            voteDialog.setCancelable(true);
            //voteDialog.setDialogTitle("Add a connotation to this word");
            voteDialog.show(manager,"Vote Dialog");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Shows the received result from the dialog fragment.
     * @param voteInput the vote the user inputted
     */
    public void onFinishResultDialog(String voteInput) {
        Log.i(TAG,"Received "+voteInput);
        Toast.makeText(this, "Received "+voteInput,Toast.LENGTH_LONG).show();
        //call method/class that stores vote string
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case R.id.voteItm:
                sendToVote();
                return true;
            case R.id.bookedItm:
                sendToBookmarks(view);
                return true;
            case R.id.randItm: //sends to SearchResult activity but with a piece of data that says to randomize the word
                sendSearch(true);
                return true;
            case R.id.pastItm:
                sendToPast(view);
                return true;
            case R.id.statsItm:
                sendToStats(view);
                return true;
            case R.id.aboutItm:
                sendToAbout(view);
                return true;
            case R.id.settingItm:
                sendToSettings(view);
                return true;
            case R.id.helpItm:
                sendToHelp(view);
                return true;
            case R.id.whyItm:
                sendToWhyApp(view);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    /**
     * Sends the user to the Bookmarks activity.
     * @param view the current view
     */
    public void sendToBookmarks(View view) {
        Intent intent = new Intent(this, Bookmarks.class);
        startActivity(intent);
    }

    /**
     * Sends the user to the WhyApp activity.
     * @param view the current view
     */
    public void sendToWhyApp(View view) {
        Intent intent = new Intent(this, WhyApp.class);
        startActivity(intent);
    }

    /**
     * Sends the user to the Help activity.
     * @param view the current view
     */
    public void sendToHelp(View view) {
        Intent intent = new Intent(this, Help.class);
        startActivity(intent);
    }

    /**
     * Sends the user to the ContactUs activity.
     * @param view the current view
     */
    public void sendToAbout(View view) {
        Intent intent = new Intent(this, ContactUs.class);
        startActivity(intent);
    }

    /**
     * Sends the user to the Stats activity.
     * @param view the current view
     */
    public void sendToStats(View view) {
        Intent intent = new Intent(this, Stats.class);
        //add extra data word currently on if it is already on (ex: SearchResult)
        startActivity(intent);
    }

    /**
     * Sends the user to the Settings activity.
     * @param view the current view
     */
    public void sendToSettings(View view) {
        Intent intent = new Intent(this, Settings.class);
        startActivity(intent);
    }

    /**
     * Sends the user to the History activity.
     * @param view the current view
     */
    public void sendToPast(View view) {
        Intent intent = new Intent(this, History.class);
        startActivity(intent);
    }
}
