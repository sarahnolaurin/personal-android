package sarah.connotation.view;

import android.app.SearchManager;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import sarah.connotation.R;
import sarah.connotation.service.DictionaryAPI;
import sarah.connotation.view.secondary.VoteDialog;
import sarah.connotation.view.secondary.VotingInputListener;

import static java.security.AccessController.getContext;


public class MainActivity extends AppCompatActivity implements VotingInputListener,TextToSpeech.OnInitListener {
    private static final String TAG = "MAIN";
    View view;
    TextToSpeech tts;
    private SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Get the intent, verify the action and get the query
        Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            sendSearch(query);
        }

        registerForContextMenu(findViewById(R.id.connotationDefinition));
        registerForContextMenu(findViewById(R.id.connotation));

    }



    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.bookmrks_context_menu, menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.searchItm);
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.wtf(TAG, query);
                sendSearch(query);
                searchView.clearFocus();
                return true;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        return true;
    }

    @Override
    public void onInit(int i) {
        ArrayList<String> activityText = new ArrayList<>();
        //add the strings
        activityText.add(getResources().getString(R.string.txtConnotation));
        activityText.add(getResources().getString(R.string.txtConnotationDefinition));
        activityText.add(getResources().getString(R.string.txtExample1));
        activityText.add(getResources().getString(R.string.txtExample2));
        activityText.add(getResources().getString(R.string.txtExample3));
        activityText.add(getResources().getString(R.string.txtExample7));

        tts.setLanguage(Locale.US);
        //loop through the strings to add them to tts
        for(int j = 0; j < activityText.size(); j++) {
            tts.speak(activityText.get(j), TextToSpeech.QUEUE_ADD, new Bundle(), "HomeActivity");
        }
    }

    /**
     * Starts the text-to-speech service.
     * @param view the button's view
     */
    public void speakText(View view) {
        tts = new TextToSpeech(getApplicationContext(), this);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
    }

    /**
     * Send the search query to the SearchResult class.
     * @param query the searched query
     */
    public void sendSearch(String query) {
        Intent intent = new Intent(this, SearchResult.class);
        if(query.length() > 0) {
            intent.putExtra("search_input", query);
        }
        else {
            sendSearch(true);
        }
        startActivity(intent);
    }

    /**
     * Send a search query for a random word
     * @param isRandom value that determines whether the search should choose a random word or not.
     */
    public void sendSearch(Boolean isRandom) {
        Intent intent = new Intent(this, SearchResult.class);
        if(isRandom)
            intent.putExtra("isRandom", true);
        startActivity(intent);
    }

    /**
     * Sends the user to a dialog fragment to vote the connotation of a word.
     */
    public void sendToVote() {
        try {
            FragmentManager manager = getSupportFragmentManager();
            VoteDialog voteDialog = new VoteDialog();
            voteDialog.setCancelable(true);
            //voteDialog.setDialogTitle("Add a connotation to this word");
            voteDialog.show(manager,"Vote Dialog");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Shows the received result from the dialog fragment.
     * @param voteInput the vote the user inputted
     */
    public void onFinishResultDialog(String voteInput) {
        Toast.makeText(this, "Received vote: "+voteInput,Toast.LENGTH_LONG).show();
        //call method/class that stores vote string
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case R.id.voteItm:
                sendToVote();
                return true;
            case R.id.bookedItm:
                sendToBookmarks(view);
                return true;
            case R.id.randItm: //sends to SearchResult activity but with a piece of data that says to randomize the word
                sendSearch(true);
                return true;
            case R.id.pastItm:
                sendToPast(view);
                return true;
            case R.id.statsItm:
                sendToStats(view);
                return true;
            case R.id.aboutItm:
                sendToAbout(view);
                return true;
            case R.id.settingItm:
                sendToSettings(view);
                return true;
            case R.id.helpItm:
                sendToHelp(view);
                return true;
            case R.id.whyItm:
                sendToWhyApp(view);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.connotation:
                copyText();
                return true;
            case R.id.connotationDefinition:
                copyText();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    /**
     * Copy text.
     */
    public void copyText() {
        try {
            ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
            TextView definition = view.findViewById(R.id.word_bookmarked);
            ClipData clip = ClipData.newPlainText("", definition.getText().toString());
            clipboard.setPrimaryClip(clip);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sends the user to the Bookmarks activity.
     * @param view the current view
     */
    public void sendToBookmarks(View view) {
        Intent intent = new Intent(this, Bookmarks.class);
        startActivity(intent);
    }

    /**
     * Sends the user to the WhyApp activity.
     * @param view the current view
     */
    public void sendToWhyApp(View view) {
        Intent intent = new Intent(this, WhyApp.class);
        startActivity(intent);
    }

    /**
     * Sends the user to the Help activity.
     * @param view the current view
     */
    public void sendToHelp(View view) {
        Intent intent = new Intent(this, Help.class);
        startActivity(intent);
    }

    /**
     * Sends the user to the ContactUs activity.
     * @param view the current view
     */
    public void sendToAbout(View view) {
        Intent intent = new Intent(this, ContactUs.class);
        startActivity(intent);
    }

    /**
     * Sends the user to the Stats activity.
     * @param view the current view
     */
    public void sendToStats(View view) {
        Intent intent = new Intent(this, Stats.class);
        //add extra data word currently on if it is already on (ex: SearchResult)
        startActivity(intent);
    }

    /**
     * Sends the user to the Settings activity.
     * @param view the current view
     */
    public void sendToSettings(View view) {
        Intent intent = new Intent(this, Settings.class);
        startActivity(intent);
    }

    /**
     * Sends the user to the History activity.
     * @param view the current view
     */
    public void sendToPast(View view) {
        Intent intent = new Intent(this, History.class);
        startActivity(intent);
    }
}

