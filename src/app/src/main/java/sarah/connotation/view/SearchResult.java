package sarah.connotation.view;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.nikhilpanju.recyclerviewenhanced.OnActivityTouchListener;
import com.nikhilpanju.recyclerviewenhanced.RecyclerTouchListener;

import org.w3c.dom.Text;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;

import sarah.connotation.R;
import sarah.connotation.controller.BookmarkController;
import sarah.connotation.controller.ConnotationController;
import sarah.connotation.model.Connotations;
import sarah.connotation.service.DictionaryAPI;
import sarah.connotation.service.DictionaryAPICallback;
import sarah.connotation.view.recycler.ScaleAdapter;
import sarah.connotation.view.recycler.ScaleViewHolder;
import sarah.connotation.view.secondary.VoteDialog;

public class SearchResult extends AppCompatActivity {
    private static final String TAG = "SEARCH_RESULT";
    private View view;
    private TextView txtWord;
    private TextView txtDefinition;
    private String receivedWord;
    private Connotations connotation;
    private SearchView searchView;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter<ScaleViewHolder> adapter;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerTouchListener onTouchListener;
    private OnActivityTouchListener activityTouchListener;
    private final int WEB_REQ_CODE = 1515;

    //TODO: LET USER ADD A VOTE TO THE WORD PAGE AT ANY TIME
          //THIS SHOULD MAKE SURE THE USER DOES NOT ENTER A VOTE TWICE

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // GET INTENT, IF RECEIVE SEARCH RESULT DATA, READ DATABASE
        // AND MAYBE HAVE A PROGRESS BAR
        // THEN DISPLAY THE CONTENT OF THE SEARCH
        Intent intent = getIntent();
        if(intent.hasExtra("search_input")) {
            receivedWord = intent.getStringExtra("search_input");
            deploySearch(intent);
        }
        else if(intent.hasExtra("isRandom"))
            deploySearch(intent);
        Log.wtf(TAG, receivedWord);
    }

    /**
     * Searches for the word in the database and in the api if needed, then shows the results on the activity.
     * @param intent The intent that contains the search query and/or the random value.
     */
    public void deploySearch(Intent intent) { //TODO: check for the user's language and change the api url for it
        ConnotationController connotationController = new ConnotationController(this);
        connotation = new Connotations();

        //new DeployAsyncSearch().execute(intent);

        try { //TODO: make an asyncTask out of this?
            if(!intent.hasExtra("isRandom")) {
                connotation = connotationController.getSearchResults(receivedWord);
                if(connotation.getWord() == null) {
                    Log.wtf(TAG,"connotation is null");
                    DictionaryAPI api = new DictionaryAPI(receivedWord);
                    api.setContext(this);
                    api.sendRequest(new DictionaryAPICallback() {
                        @Override
                        public void onSuccess(String word, String definition) {
                            TextView txtWord = (TextView) findViewById(R.id.searched_word);
                            TextView txtDefinition = (TextView) findViewById(R.id.definition);
                            txtDefinition.setText(definition);
                            txtWord.setText(word);
                            ConnotationController subController = new ConnotationController(getApplicationContext());
                            subController.insertNewWord(word,definition);
                        }
                    });
                }
                else {
                    Log.wtf(TAG,"connotation is not null");
                    TextView txtWord = (TextView) findViewById(R.id.searched_word);
                    TextView txtDefinition = (TextView) findViewById(R.id.definition);
                    txtDefinition.setText(connotation.getDefinition());
                    txtWord.setText(connotation.getWord());
                    Log.wtf(TAG, "connotation votes: "+connotation.getConnotationVotes());

                    if (connotation.getConnotationVotes() != null) { //SHOWS SOMETHING, BUT MUST FIX ADAPTER/VIEWHOLDER
                        this.recyclerView = (RecyclerView) findViewById(R.id.scale_recycler);
                        this.layoutManager = new LinearLayoutManager(this);
                        this.recyclerView.setLayoutManager(layoutManager);
                        HashMap<String, Integer> wordVotes = connotation.getConnotationVotes();
                        this.adapter = new ScaleAdapter(wordVotes);
                        this.recyclerView.setAdapter(adapter);
                    }
                    else {
                        //TODO: SHOW EMPTY MESSAGE WHEN THERE ARE NO VOTES YET
                    }
                }
            }
            else { //TODO : this part is for RANDOM WORDS, FIX THIS LATER
                //THIS SHOULD WORK
                connotation = connotationController.getSearchResults(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //this is done in case connotation is null (can put this in the above to not make it redundant)
        txtWord = (TextView) findViewById(R.id.searched_word);
        txtDefinition = (TextView) findViewById(R.id.definition);
        connotation.setWord(receivedWord);
        connotation.setDefinition(txtDefinition.getText().toString());

        Log.wtf(TAG,"deploy search found: "+connotation.getWord()+" - "+connotation.getDefinition());
    }

    private class DeployAsyncSearch extends AsyncTask<Intent, ProgressBar, String> {

        @Override
        protected String doInBackground(Intent... intents) {
            

            return null;
        }
    }

    /**
     * Send the user to a web view showing more definitions on the word shown.
     * @param view the button view
     */
    public void sendMoreDefinitions(View view) {
        txtWord = (TextView) findViewById(R.id.searched_word);
        String word = txtWord.getText().toString();
        Uri webpage = Uri.parse("https://glosbe.com/en/en/"+word);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        startActivityForResult(intent, WEB_REQ_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode,resultCode,intent);
        Toast.makeText(this, "Please vote for a connotation!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu_with_bookmark, menu);

        MenuItem searchItem = menu.findItem(R.id.searchItm);
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                sendSearch(query);
                searchView.clearFocus();
                return true;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        return true;
    }

    /**
     * Send the search query to the SearchResult class.
     * @param query the searched query
     */
    public void sendSearch(String query) {
        Intent intent = new Intent(this, SearchResult.class);
        intent.putExtra("search_input",query);
        startActivity(intent);
    }

    /**
     * Send a search query for a random word
     * @param isRandom value that determines whether the search should choose a random word or not.
     */
    public void sendSearch(Boolean isRandom) {
        Intent intent = new Intent(this, SearchResult.class);
        intent.putExtra("isRandom", true);
        finish();
        startActivity(intent);
    }

    /**
     * Sends the user to a dialog fragment to vote the connotation of a word.
     */
    public void sendToVote() {
        try {
            FragmentManager manager = getSupportFragmentManager();
            VoteDialog voteDialog = new VoteDialog();
            voteDialog.setCancelable(true);
            //voteDialog.setDialogTitle("Add a connotation to this word");
            voteDialog.show(manager,"Vote Dialog");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Shows the received result from the dialog fragment.
     * @param voteInput the vote the user inputted
     */
    public void onFinishResultDialog(String voteInput) {
        Toast.makeText(this, "Received "+voteInput,Toast.LENGTH_LONG).show();
        //call method/class that stores vote string
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case R.id.voteItm:
                sendToVote();
                return true;
            case R.id.searchItm:
                sendSearch(true);//onSearchRequested();
                return true;
            case R.id.bookedItm:
                sendToBookmarks(view);
                return true;
            case R.id.randItm: //sends to SearchResult activity but with a piece of data that says to randomize the word
                sendSearch(true);
                return true;
            case R.id.pastItm:
                sendToPast(view);
                return true;
            case R.id.statsItm:
                sendToStats(view);
                return true;
            case R.id.aboutItm:
                sendToAbout(view);
                return true;
            case R.id.settingItm:
                sendToSettings(view);
                return true;
            case R.id.helpItm:
                sendToHelp(view);
                return true;
            case R.id.whyItm:
                sendToWhyApp(view);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.connotation:
                copyText();
                return true;
            case R.id.connotationDefinition:
                copyText();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    /**
     * Lets the user copy the text they selected.
     */
    public void copyText() {
        try {
            ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
            TextView definition = view.findViewById(R.id.word_bookmarked);
            ClipData clip = ClipData.newPlainText("", definition.getText().toString());
            clipboard.setPrimaryClip(clip);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sends the user to the Bookmarks activity.
     * @param view the current view
     */
    public void sendToBookmarks(View view) {
        Intent intent = new Intent(this, Bookmarks.class);
        startActivity(intent);
    }

    /**
     * Sends the user to the WhyApp activity.
     * @param view the current view
     */
    public void sendToWhyApp(View view) {
        Intent intent = new Intent(this, WhyApp.class);
        startActivity(intent);
    }

    /**
     * Sends the user to the Help activity.
     * @param view the current view
     */
    public void sendToHelp(View view) {
        Intent intent = new Intent(this, Help.class);
        startActivity(intent);
    }

    /**
     * Sends the user to the ContactUs activity.
     * @param view the current view
     */
    public void sendToAbout(View view) {
        Intent intent = new Intent(this, ContactUs.class);
        startActivity(intent);
    }

    /**
     * Sends the user to the Stats activity.
     * @param view the current view
     */
    public void sendToStats(View view) {
        Intent intent = new Intent(this, Stats.class);
        //add extra data word currently on if it is already on (ex: SearchResult)
        startActivity(intent);
    }

    /**
     * Sends the user to the Settings activity.
     * @param view the current view
     */
    public void sendToSettings(View view) {
        Intent intent = new Intent(this, Settings.class);
        startActivity(intent);
    }

    /**
     * Sends the user to the History activity.
     * @param view the current view
     */
    public void sendToPast(View view) {
        Intent intent = new Intent(this, History.class);
        startActivity(intent);
    }

    /**
     * Adds or removes the word from the bookmarks.
     * @param view the button's view
     */
    public void pressBookmark(View view) { //TODO: FIX THIS
        //changes the icon of the menu item
        Drawable bookmarkBorder = getResources().getDrawable(R.drawable.ic_bookborder, getTheme());
        Drawable bookmarkFull = getResources().getDrawable(R.drawable.ic_bookfull, getTheme());
        BookmarkController controller = new BookmarkController(this);
        ImageButton bookmrk = view.findViewById(R.id.bookmark_button);
        txtWord = (TextView) findViewById(R.id.searched_word);

        if(bookmrk.getDrawable().equals(bookmarkBorder)) {
            //add bookmark
            controller.addBookmark(txtWord.getText().toString());
            bookmrk.setImageDrawable(bookmarkFull);
        }
        else if (bookmrk.getDrawable().equals(bookmarkFull)) {
            //remove bookmark
            String wordToRemove = txtWord.getText().toString();
            int wordIdToRemove = controller.getWordNumberAssociatedToWordName(wordToRemove,false);
            controller.removeBookMark(wordIdToRemove);
            bookmrk.setImageDrawable(bookmarkBorder);
        }
    }
}
