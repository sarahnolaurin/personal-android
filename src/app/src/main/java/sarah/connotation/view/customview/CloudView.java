package sarah.connotation.view.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import sarah.connotation.R;

public class CloudView extends View {
    private int width = 70;
    private int height = 70;
    Paint paintShape;
    private int color;

    public CloudView(Context context) {
        super(context);
    }
    public CloudView(Context context, AttributeSet attrs) {
        super(context,attrs);
        setupAttributes(attrs);
        setupPaint();
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawCircle(width / 2, height / 2, width / 2, paintShape);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int minimumWidth = width + getPaddingLeft() + getPaddingRight();
        int tempWidth = resolveSizeAndState(minimumWidth, widthMeasureSpec, 0);
        int minimumHeight = height + getPaddingBottom() + getPaddingTop();
        int tempHeight = resolveSizeAndState(minimumHeight, heightMeasureSpec, 0);

        setMeasuredDimension(tempWidth, tempHeight);
    }

    private void setupAttributes(AttributeSet attrs) {
        // Obtain a typed array of attributes
        TypedArray a = getContext().getTheme().obtainStyledAttributes(attrs, R.styleable.CloudView, 0, 0);
        // Extract custom attributes into member variables
        try {
            color = a.getColor(R.styleable.CloudView_shapeColor, Color.BLACK);
        } finally {
            // TypedArray objects are shared and must be recycled.
            a.recycle();
        }
    }

    public int getShapeColor() {
        return color;
    }

    public void setShapeColor(int color) {
        this.color = color;
        invalidate();
        requestLayout();
    }

    private void setupPaint() {
        paintShape = new Paint();
        paintShape.setStyle(Paint.Style.FILL);
        paintShape.setColor(color);
    }
}
