package sarah.connotation.view.recycler;


import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import sarah.connotation.R;
import sarah.connotation.controller.BookmarkController;
import sarah.connotation.model.BookmarkModel;
import sarah.connotation.view.Bookmarks;
import sarah.connotation.view.SearchResult;

public class BookmarkViewHolder extends RecyclerView.ViewHolder {
    private static final String TAG = "BOOKMARKVIEWHOLDER";

    private TextView bookmarkedWord;
    private ImageButton deleteButton;
    private BookmarkModel bookmark;


    public BookmarkViewHolder(View itemView) {
        super(itemView);
       bookmarkedWord = (TextView) itemView.findViewById(R.id.word_bookmarked);
//        deleteButton = (ImageButton) itemView.findViewById(R.id.btn_delete_bookmark);
//        final BookmarkController bookmarkController = new BookmarkController(itemView.getContext());
        /*
        bookmarkedWord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get the context
                Context context = v.getContext();
                //Intent intent = new Intent(this, SearchResult.class);
                //intent.putExtra("search_input", bookmarkedWord.getText().toString());

            }
        });
        deleteButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                bookmarkController.removeBookMark(bookmark.getBookmarkId());
            }
        });
        */
    }

    public void setBookmark(BookmarkModel bookmark) {
        this.bookmark = bookmark;
        this.bookmarkedWord.setText(bookmark.getWord());
    }
}
