package sarah.connotation.view.recycler;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import sarah.connotation.R;
import sarah.connotation.model.BookmarkModel;

public class BookmarksAdapter extends RecyclerView.Adapter<BookmarkViewHolder> {

    private ArrayList<BookmarkModel> bookmarks;

    public BookmarksAdapter (ArrayList<BookmarkModel> bookmark) {
        this.bookmarks = bookmark;
    }


    @Override
    public BookmarkViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Get the parent view's context
        Context context = parent.getContext();
        // Inflate the layout_item
        View view = LayoutInflater.from(context).inflate(R.layout.bookmark_item, parent, false);
        // Create a ViewHolder
        BookmarkViewHolder viewHolder = new BookmarkViewHolder(view);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(BookmarkViewHolder holder, int position) {
        // Get the movie at the given position
        BookmarkModel bookmark = bookmarks.get(position);
        // Set the movie to the ViewHolder
        holder.setBookmark(bookmark);
    }

    @Override
    public int getItemCount() {
        // Return the size of the data
        return bookmarks.size();
    }
}
