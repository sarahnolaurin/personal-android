package sarah.connotation.view.recycler;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import sarah.connotation.R;
import sarah.connotation.model.PastVotesModel;

/**
 * The type History adapter.
 */
public class HistoryAdapter extends RecyclerView.Adapter<HistoryViewHolder> {

    private ArrayList<PastVotesModel> pastVotes;

    public HistoryAdapter(ArrayList<PastVotesModel> pastVote) {
        this.pastVotes = pastVote;
    }

    /**
     * This method is called by LayoutManager to create a new method.
     */
    @Override
    public HistoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Get the parent view's context
        Context context = parent.getContext();
        // Inflate the layout_item
        View view = LayoutInflater.from(context).inflate(R.layout.history_item, parent, false);
        // Create a ViewHolder
        HistoryViewHolder viewHolder = new HistoryViewHolder(view);
        return viewHolder;
    }
    /**
     * This method is called by LayoutManager to bind the ViewHolder with required data.
     */
    @Override
    public void onBindViewHolder(HistoryViewHolder holder, int position) {
        // Get the movie at the given position
        PastVotesModel pastVote = pastVotes.get(position);
        // Set the movie to the ViewHolder
        holder.setPastVote(pastVote);
    }

    @Override
    public int getItemCount() {
        // Return the size of the data
        return pastVotes.size();
    }
}
