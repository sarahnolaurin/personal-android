package sarah.connotation.view.recycler;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import sarah.connotation.R;
import sarah.connotation.controller.PastVotesController;
import sarah.connotation.model.PastVotesModel;

public class HistoryViewHolder extends RecyclerView.ViewHolder {
    private PastVotesModel pastVote;
    private TextView givenWord;
    private TextView userVote;

    public HistoryViewHolder(View itemView) {
        super(itemView);
        Context context = itemView.getContext();

        this.givenWord = (TextView) itemView.findViewById(R.id.word_given);
        this.userVote = (TextView) itemView.findViewById(R.id.word_voted);
        final String oldVote = userVote.getText().toString();
        final PastVotesController controller = new PastVotesController(context);

                //update text
                //userVote = (EditText) view.findViewById(R.id.word_voted);
                //String newVote = userVote.getText().toString();
                //controller.updateVote(oldVote, newVote);
                //add more functionality (validation, auto-correct, suggestion) once this works
                //                      and a timer/timestamp for a year

    }

    public void setPastVote(PastVotesModel vote) {
        this.pastVote = vote;

        this.givenWord.setText(pastVote.getWordGiven());
        this.userVote.setText(pastVote.getWordVoted());
    }
}
