package sarah.connotation.view.recycler;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

import sarah.connotation.R;
import sarah.connotation.model.BookmarkModel;
import sarah.connotation.model.Connotations;

public class ScaleAdapter  extends RecyclerView.Adapter<ScaleViewHolder>{
    private HashMap<String, Integer> wordVotes;
    public ScaleAdapter(HashMap<String, Integer> wordVotes) {
        sortHashMap(wordVotes);
        this.wordVotes = wordVotes;
    }

    @Override
    public ScaleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Get the parent view's context
        Context context = parent.getContext();
        // Inflate the layout_item
        View view = LayoutInflater.from(context).inflate(R.layout.scale_list_item, parent, false);
        // Create a ViewHolder
        ScaleViewHolder viewHolder = new ScaleViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ScaleViewHolder holder, int position) {
        // Get the values at the given position
        String[] words = wordVotes.keySet().toArray(new String[wordVotes.size()]);
        Integer[] votes = wordVotes.values().toArray(new Integer[wordVotes.size()]);
        String word = words[position];
        int vote = votes[position];
        Log.wtf("ADAPTER", "here : " + word + "  "+vote);
        // Set the values to the ViewHolder
        holder.setWordVotes(word, vote, votes);
    }

    @Override
    public int getItemCount() {
        return wordVotes.size();
    }

    /**
     * Sorts the HashMap according to the vote numbers in decreasing order.
     * @param wordVotes
     * @return
     */
    public HashMap<String, Integer> sortHashMap(HashMap<String, Integer> wordVotes) {
        String[] words = wordVotes.keySet().toArray(new String[wordVotes.size()]);
        Integer[] votes = wordVotes.values().toArray(new Integer[wordVotes.size()]);

        // sorts the hash map in descending order
        for(int i = 0; i < votes.length-1 & i < words.length-1; i++) {
            for(int j = i +1; j < votes.length & j < words.length; j++) {
                if(votes[i] < votes[j]) {
                    int tempVote = votes[i];
                    votes[i] = votes[j];
                    votes[j] = tempVote;

                    String tempWord = words[i];
                    words[i] = words[j];
                    words[j] = tempWord;
                }
            }
        }
        wordVotes = new HashMap<>();
        for(int i = 0; i < votes.length; i++) {
            wordVotes.put(words[i],votes[i]);
        }
        return wordVotes;
    }
}
