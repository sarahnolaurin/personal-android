package sarah.connotation.view.recycler;


import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import sarah.connotation.R;

public class ScaleViewHolder extends RecyclerView.ViewHolder {

    private String word;
    private int votes;
    private Integer[] voteList;
    TextView connotation;
    ProgressBar scale;

    public ScaleViewHolder(View itemView) {
        super(itemView);

        connotation = (TextView) itemView.findViewById(R.id.connotation);
        scale = (ProgressBar) itemView.findViewById(R.id.scale);

    }

    public void setWordVotes(String word, int votes, Integer[] voteList) {
        this.word = word;
        this.votes = votes;
        this.voteList = voteList;

        connotation.setText(word);
        scale.setProgress(computeProgress(voteList, votes));
    }

    /**
     * Gets scale number.
     *
     * @param voteList the votes for all the other words
     * @param vote the vote for this one word
     * @return the scale number
     */
    public int computeProgress(Integer[] voteList, int vote) {
        int progress = vote;
        //calculations here
        //int smallest = voteList[voteList.length-1];
        //int largest = voteList[0];

        return progress;
    }
}
