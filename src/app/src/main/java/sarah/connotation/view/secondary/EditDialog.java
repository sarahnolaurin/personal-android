package sarah.connotation.view.secondary;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import sarah.connotation.R;
import sarah.connotation.controller.ConnotationController;
import sarah.connotation.controller.PastVotesController;
import sarah.connotation.model.Connotations;
import sarah.connotation.view.History;

import static sarah.connotation.view.secondary.VoteDialog.dialogboxTitle;

public class EditDialog extends DialogFragment {

    private static final String TAG = "VOTEDIALOG";
    TextView question;
    EditText inputVote;
    Button btnSubmit;
    Button btnCancel;

    public EditDialog () {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialogStyle);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if(dialog != null){
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            dialog.getWindow().setLayout(width,height);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        final View view = inflater.inflate(R.layout.vote_view,container,false);
        question = (TextView) view.findViewById(R.id.question);
        question.setText(getResources().getString(R.string.editText));
        btnSubmit = (Button) view.findViewById(R.id.submitButton);
        btnCancel = (Button) view.findViewById(R.id.cancelButton);
        inputVote = (EditText) view.findViewById(R.id.answer);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String newVote = inputVote.getText().toString();
                    //if(validateInput(vote)) {
                    PastVotesController controller = new PastVotesController(getContext());
                    controller.updateVote(getOldVote(view), newVote);
                    //}
                    dismiss();
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        getDialog().setTitle(dialogboxTitle);
        return view;
    }

    public boolean validateInput(String input) {
        if(input.length() <= 0 && (input.matches(".*\\d+.*"))) {
            Log.wtf(TAG,"Invalid data entered");
            return false;
        }
        else {
            return true;
        }
    }

    public String getOldVote(View view) {
        TextView oldVoteView = (TextView) view.findViewById(R.id.word_voted);
        String oldVote = oldVoteView.getText().toString();
        return oldVote;
    }

    /**
     * Sets the title of the Dialog box
     * @param title The title to be set.
     */
    public void setDialogTitle(String title) {
        dialogboxTitle = title;
    }
}
