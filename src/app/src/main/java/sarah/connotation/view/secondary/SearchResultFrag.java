package sarah.connotation.view.secondary;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nikhilpanju.recyclerviewenhanced.OnActivityTouchListener;
import com.nikhilpanju.recyclerviewenhanced.RecyclerTouchListener;


import java.io.IOException;
import java.util.HashMap;

import sarah.connotation.R;
import sarah.connotation.controller.BookmarkController;
import sarah.connotation.controller.ConnotationController;
import sarah.connotation.model.Connotations;
import sarah.connotation.service.DictionaryAPI;
import sarah.connotation.view.recycler.ScaleAdapter;
import sarah.connotation.view.recycler.ScaleViewHolder;


public class SearchResultFrag extends Fragment {
    private static final String TAG = "SEARCHRESULTFRAG";
    private ImageButton btnBookmark;
    private TextView txtWord;
    private TextView txtDefinition;
    private String receivedWord;
    private Boolean isRandom;
    private Connotations connotation;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter<ScaleViewHolder> adapter;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerTouchListener onTouchListener;
    private OnActivityTouchListener activityTouchListener;

    /**
     * Instantiates a new Search result frag.
     */
    public SearchResultFrag() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.search_frag, container, false);
        btnBookmark = (ImageButton) view.findViewById(R.id.bookmark_button);
        txtDefinition = (TextView) view.findViewById(R.id.definition);
        txtWord = (TextView) view.findViewById(R.id.searched_word);

        generateWordsAndScales(view);
        return view;
    }

    /**
     * Sets random.
     *
     * @param rand the rand
     */
    public void setRandom(Boolean rand) {
        isRandom = rand;
    }


    /**
     * Generate words and scales.
     *
     * @param view the view
     */
    public void generateWordsAndScales(View view) {
        try {
//            ConnotationController controller = new ConnotationController(getContext());
//            Connotations connotations;
//            int wordId = controller.getWordNumberAssociatedToWordName(this.receivedWord,false);
//            connotations = controller.getSearchResults(wordId);
//            Log.wtf(TAG,connotations.getWord());
//            txtWord = view.findViewById(R.id.searched_word);
//            txtDefinition = view.findViewById(R.id.definition);
//            scales_container = view.findViewById(R.id.scales_container);
//            //generate results
//            txtWord.setText(connotations.getWord());
//            txtDefinition.setText(connotations.getDefinition());
//            HashMap<String,Integer> wordVotes = connotations.getConnotationVotes();
//            ArrayList<String> wordsVoted = (ArrayList<String>) wordVotes.keySet();
//
//        for (int i = 0; i < wordVotes.size(); i++) {
//            ProgressBar scale = new ProgressBar(getContext());
//
//
//            TextView connotation = new TextView(getContext());
//
//
//            scales_container.addView(scale);
//            scales_container.addView(connotation);
//        }

        } catch (Exception e) {
            e.printStackTrace();
        }
        //figure out how many scales to show in search results: HashMapName.size()
        //calculate the scales in search results: percentage
        //number of votes per word is automatically added each time someone votes
    }



    /**
     * Sets word.
     *
     * @param word the word
     */
    public void setWord(String word) {
        receivedWord = word;
    }
}