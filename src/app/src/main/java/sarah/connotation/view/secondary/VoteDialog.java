package sarah.connotation.view.secondary;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import sarah.connotation.R;
import sarah.connotation.controller.ConnotationController;
import sarah.connotation.model.Connotations;

import static android.content.Context.MODE_PRIVATE;

public class VoteDialog extends DialogFragment {
    private static final String TAG = "VOTEDIALOG";
    private Connotations connotation;
    TextView wordVoteView;
    EditText inputVote;
    Button btnSubmit;
    Button btnCancel;
    static String dialogboxTitle;

    public VoteDialog() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialogStyle);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if(dialog != null){
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            dialog.getWindow().setLayout(width,height);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.vote_view,container,false);
        connotation = setRandomWord(view);

        btnSubmit = (Button) view.findViewById(R.id.submitButton);
        btnCancel = (Button) view.findViewById(R.id.cancelButton);
        inputVote = (EditText) view.findViewById(R.id.answer);

//        SharedPreferences prefs = getActivity().getPreferences(MODE_PRIVATE);
//        String input = prefs.getString("value", "");
//        inputVote.setText(input);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String vote = inputVote.getText().toString();
                    //if(validateInput(vote)) {
                    VotingInputListener activity = (VotingInputListener) getActivity();
                    activity.onFinishResultDialog(vote);
                    ConnotationController controller = new ConnotationController(getContext());
                    controller.addVote(connotation.getWordId(), controller.getWordNumberAssociatedToWordName(vote, false));
                    //}
                    dismiss();
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                SharedPreferences prefs = getActivity().getSharedPreferences("voted word", MODE_PRIVATE);
//                SharedPreferences.Editor prefEditor = prefs.edit();
//                prefEditor.putString("value",inputVote.getText().toString());
//                prefEditor.apply();
                dismiss();
            }
        });
        getDialog().setTitle(dialogboxTitle);


        return view;
    }

    public boolean validateInput(String input) {
        if(input.length() <= 0 && (input.matches(".*\\d+.*"))) {
            Log.wtf(TAG,"Invalid data entered");
            return false;
        }
        else {
            return true;
        }
    }

    /**
     * Retrieve a random word from the database to set to the dialog
     * @param view the view
     */
    public Connotations setRandomWord(View view) {
        Connotations connotations;
        ConnotationController controller = new ConnotationController(getContext());
        connotations = controller.getSearchResults(true);
        wordVoteView = (TextView) view.findViewById(R.id.word);
        wordVoteView.setText(connotations.getWord());
        return connotations;
    }

    /**
     * Sets the title of the Dialog box
     * @param title The title to be set.
     */
    public void setDialogTitle(String title) {
        dialogboxTitle = title;
    }
}
