package sarah.connotation.view.secondary;

public interface VotingInputListener {
    void onFinishResultDialog(String voteInput);
}
