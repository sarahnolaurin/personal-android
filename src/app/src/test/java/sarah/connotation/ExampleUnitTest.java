package sarah.connotation;

import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import sarah.connotation.controller.BookmarkController;
import sarah.connotation.model.BookmarkModel;
import sarah.connotation.service.DictionaryAPI;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {


    @Test
    public void testDefinitionJson() throws JSONException {

        String response = "{   \"result\": \"ok\",   \"tuc\": [     {       \"meanings\": [         {           \"language\": \"en\",           \"text\": \"The state of being free from physical or psychological disease, illness, or malfunction; wellness. [from 11th c.]\"         },         {           \"language\": \"en\",           \"text\": \"(obsolete) Cure, remedy. [11th-16th c.]\"         },         {           \"language\": \"en\",           \"text\": \"health (physical)\"         },         {           \"language\": \"en\",           \"text\": \"the general condition of body and mind; &quot;his delicate health&quot;; &quot;in poor health&quot;\"         },         {           \"language\": \"en\",           \"text\": \"(countable) A toast to prosperity. [from 17th c.]\"         },         {           \"language\": \"en\",           \"text\": \"health(y)\"         },         {           \"language\": \"en\",           \"text\": \"A warrior; hero; man.\"         },         {           \"language\": \"en\",           \"text\": \"A toast to prosperity. [from 17th c.]\"         },         {           \"language\": \"en\",           \"text\": \"A state of dynamic equilibrium between an organism and its environment in which all functions of mind and body are normal.\"         },         {           \"language\": \"en\",           \"text\": \"physical condition\"         },         {           \"language\": \"en\",           \"text\": \"Information about a client computer that Network Access Protection (NAP) uses to allow or deny access to a network and which is encapsulated in a statement of health (SoH), which is issued by a system health agent (SHA) on the client computer.\"         },         {           \"language\": \"en\",           \"text\": \"A state of well-being or balance, often physical but sometimes also mental and social; the overall level of function of an organism from the cellular (micro) level to the social (macro) level.\"         },         {           \"language\": \"en\",           \"text\": \"Cure, remedy. [11th-16th c.]\"         },         {           \"language\": \"en\",           \"text\": \"Physical condition.\"         },         {           \"language\": \"en\",           \"text\": \"state of being free of physical or psychological disease, illness, or malfunction\"         },         {           \"language\": \"en\",           \"text\": \"a healthy state of wellbeing free from disease; &quot;physicians should be held responsible for the health of their patients&quot;\"         },         {           \"language\": \"en\",           \"text\": \"overall level of function of an organism\"         }       ],       \"meaningId\": null,       \"authors\": [         1       ]     }  ]    }";
        //String response = "{ \"name\":\"John\", \"age\":30, \"car\":null }";
        JSONObject jsonObject = new JSONObject(response);
        DictionaryAPI controller = new DictionaryAPI("random");
        String printMe = controller.getDefinitionFromResponse(jsonObject);
        //System.out.println("json"+ response.toString());

    }

    @Test
    public void testWordJson() throws JSONException {
        String response = "{   \"result\": \"ok\",   \"tuc\": [     {       \"meanings\": [         {           \"language\": \"en\",           \"text\": \"The state of being free from physical or psychological disease, illness, or malfunction; wellness. [from 11th c.]\"         },         {           \"language\": \"en\",           \"text\": \"(obsolete) Cure, remedy. [11th-16th c.]\"         },         {           \"language\": \"en\",           \"text\": \"health (physical)\"         },         {           \"language\": \"en\",           \"text\": \"the general condition of body and mind; &quot;his delicate health&quot;; &quot;in poor health&quot;\"         },         {           \"language\": \"en\",           \"text\": \"(countable) A toast to prosperity. [from 17th c.]\"         },         {           \"language\": \"en\",           \"text\": \"health(y)\"         },         {           \"language\": \"en\",           \"text\": \"A warrior; hero; man.\"         },         {           \"language\": \"en\",           \"text\": \"A toast to prosperity. [from 17th c.]\"         },         {           \"language\": \"en\",           \"text\": \"A state of dynamic equilibrium between an organism and its environment in which all functions of mind and body are normal.\"         },         {           \"language\": \"en\",           \"text\": \"physical condition\"         },         {           \"language\": \"en\",           \"text\": \"Information about a client computer that Network Access Protection (NAP) uses to allow or deny access to a network and which is encapsulated in a statement of health (SoH), which is issued by a system health agent (SHA) on the client computer.\"         },         {           \"language\": \"en\",           \"text\": \"A state of well-being or balance, often physical but sometimes also mental and social; the overall level of function of an organism from the cellular (micro) level to the social (macro) level.\"         },         {           \"language\": \"en\",           \"text\": \"Cure, remedy. [11th-16th c.]\"         },         {           \"language\": \"en\",           \"text\": \"Physical condition.\"         },         {           \"language\": \"en\",           \"text\": \"state of being free of physical or psychological disease, illness, or malfunction\"         },         {           \"language\": \"en\",           \"text\": \"a healthy state of wellbeing free from disease; &quot;physicians should be held responsible for the health of their patients&quot;\"         },         {           \"language\": \"en\",           \"text\": \"overall level of function of an organism\"         }       ],       \"meaningId\": null,       \"authors\": [         1       ]     }  ], \"phrase\" : \"word\",\n" +
                "  \"from\" : \"en\",\n" +
                "  \"dest\" : \"en\",\n" +
                "  \"authors\" : {\n" +
                "    \"2736\" : {\n" +
                "      \"U\" : \"http://www.csse.monash.edu.au/~jwb/j_jmdict.html\",\n" +
                "      \"id\" : 2736,\n" +
                "      \"N\" : \"JMdict-Japanese-Multilingual-Dictio...\",\n" +
                "      \"url\" : \"https://glosbe.com/source/2736\"\n" +
                "    },\n" +
                "    \"84\" : {\n" +
                "      \"U\" : \"http://www.microsoft.com/Language/en-US/Terminology.aspx\",\n" +
                "      \"id\" : 84,\n" +
                "      \"N\" : \"MicrosoftLanguagePortal\",\n" +
                "      \"url\" : \"https://glosbe.com/source/84\"\n" +
                "    }\n" +
                "  }\n" +
                "}    }";
        JSONObject jsonObj = new JSONObject(response);
        DictionaryAPI controller = new DictionaryAPI("tyranny");
        String printMe = controller.getWordFromResponse(jsonObj);
        //System.out.println("json" + printMe);
    }

}